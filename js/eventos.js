$(document).ready(function() {
	$("#consultar").click(function(event) {
		$.get('http://localhost/ajax/backend/consultarProductos.php',function(data) {
			var info = JSON.parse(data);
			var productos = info.productos;
			console.log(productos);
			if(productos.length>0){
				$("#listado").html("");
				productos.forEach(imprimeProducto);
			}
		});
	});

	function imprimeProducto(item){
		var data = "<li>"+
						"<a id='detalle_p'"+ 
						"href='#detalle'"+
						"class='ui-btn ui-btn-icon-right ui-icon-carat-r'"+ 
						" data-nombre='"+item.nombre+"'"+
						" data-descripcion='"+item.descripcion+"'"+
						" data-precio='"+item.precio+"'"+
						" data-marca='"+item.marca+"'"+
						" data-id='"+item.id+"'>"+
						item.nombre+"</a>"+
						"</li>"

		$("#listado").append(data);
	}

});

$(document).on('click','#detalle_p', function(){
	$("#id").text();
	$("#nombre").text();
	$("#descripcion").text();
	$("#precio").text();
	$("#marca").text();
	$("#producto_nombre").text();

	$("#producto_nombre").text($(this).attr('data-nombre'));
	$("#id").text($(this).attr('data-id'));
	$("#nombre").text($(this).attr('data-nombre'));
	$("#descripcion").text($(this).attr('data-descripcion'));
	$("#precio").text($(this).attr('data-precio'));
	$("#marca").text($(this).attr('data-marca'));
	
	var id = $(this).attr('data-id');
	
	$("#comentarios").html("");

	$.get('http://localhost/ajax/backend/consultarComentarios.php?id='+id,function(data) {
			var info = JSON.parse(data);
			var comentarios = info.comentarios;
			console.log(comentarios);
			if(comentarios.length>0){
				$("#comentarios").html("");
				comentarios.forEach(imprimeComentarios);
			} else {
				$("#comentarios").html("No existen comentarios");
			}
		});

	function imprimeComentarios(item){
		$("#comentarios").append("<li>"+item.comentario+"<br><br></li>");
	}
});