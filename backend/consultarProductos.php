<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "Tienda2";

	$conn = new mysqli($servername, $username, $password, $db);

	$sql = "SELECT * FROM productos";
	$result = $conn->query($sql);

	$datos = array();
	$datos["error"] = "0";

	if($result->num_rows > 0){
		while($row = $result->fetch_assoc()){
			$datos["productos"][] = array("id"=>$row["id"], "nombre"=>$row["nombre"], "descripcion"=>$row["descripcion"], "precio"=>$row["precio"], "marca"=>$row["marca"]);
		}
	} else {
		$datos["error"] = "1";
	}

	$conn->close();
	echo json_encode($datos);
?>