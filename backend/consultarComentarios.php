<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "Tienda2";

	$conn = new mysqli($servername, $username, $password, $db);

	$id = $_GET["id"];

	$sql = "SELECT * FROM comentarios WHERE producto_id=$id";
	$result = $conn->query($sql);

	$datos = array();
	$datos["error"] = "0";

	if($result->num_rows > 0){
		while($row = $result->fetch_assoc()){
			$datos["comentarios"][] = array("id"=>$row["id"], "comentario"=>$row["comentario"]);
		}
	} else {
		$datos["error"] = "1";
	}

	$conn->close();
	echo json_encode($datos);

?>